#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(MACROCEPTIONCORE_LIB)
#  define MACROCEPTIONCORE_EXPORT Q_DECL_EXPORT
# else
#  define MACROCEPTIONCORE_EXPORT Q_DECL_IMPORT
# endif
#else
# define MACROCEPTIONCORE_EXPORT
#endif
