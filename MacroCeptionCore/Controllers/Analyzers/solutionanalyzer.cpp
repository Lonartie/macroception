#include "solutionanalyzer.h"

#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <fstream>

SolutionAnalyzer::SolutionAnalyzer(QObject *parent)
    : QObject(parent)
{
}

SolutionAnalyzer::~SolutionAnalyzer()
{
}

std::vector<ProjectDataBaseObject> SolutionAnalyzer::GetProjects(QString solutionPath)
{
   std::vector<ProjectDataBaseObject> projectBase;


   QFile file(solutionPath);
   assert(file.open(QIODevice::ReadOnly));
   QFileInfo info(solutionPath);
   QString parent = info.dir().canonicalPath() + "/";

   QTextStream in(&file);
   while (!in.atEnd())
   {
      QString line = in.readLine();
      if (line.contains("Project(\"{"))
      {
         QString projectPath = parent + line.split("=")[1].split(",")[1].remove("\"").remove(" ");

         if (!projectPath.contains(".vcxproj"))
            continue;

         QString projectName = line.split("=")[1].split(",")[0].remove("\"").remove(" ");
         projectBase.push_back(ProjectDataBaseObject(projectName, projectPath));
      }
   }

   file.close();

   return projectBase;
}
