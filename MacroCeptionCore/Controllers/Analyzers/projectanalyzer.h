#pragma once

#include "Common\EXPORT.h"

#include "DataObjects\Macro\macro.h"

#include <QObject>
#include <QString>
#include <QStringList>

#include <vector>


class MACROCEPTIONCORE_EXPORT ProjectAnalyzer : public QObject
{
   Q_OBJECT

public:
    ProjectAnalyzer(QObject *parent);

    static std::vector<Macro> GetMacros(const QString& projectPath);

    static QStringList GetFiles(const QString& projectPath);

    static std::vector<Macro> FindMacrosInFile(const QString& file);

    ~ProjectAnalyzer();
};
