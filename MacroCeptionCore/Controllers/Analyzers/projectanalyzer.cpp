#include "projectanalyzer.h"

#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>

ProjectAnalyzer::ProjectAnalyzer(QObject* parent)
   : QObject(parent)
{
}

std::vector<Macro> ProjectAnalyzer::GetMacros(const QString& projectPath)
{
   std::vector<Macro> macroBase;

   for (const auto& file : ProjectAnalyzer::GetFiles(projectPath))
      for (const auto& macro : ProjectAnalyzer::FindMacrosInFile(file))
         macroBase.push_back(macro);

   return macroBase;
}

QStringList ProjectAnalyzer::GetFiles(const QString& projectPath)
{
   QStringList fileList;
   QFileInfo fileInfo(projectPath);
   QString parent = fileInfo.dir().canonicalPath() + "/";

   QFile file(projectPath);
   if (!file.open(QIODevice::ReadOnly))
      return fileList;
   QTextStream in(&file);
   while (!in.atEnd())
   {
      QString line = in.readLine();

      if (line.split("\"").size() < 2)
         continue;

      if (line.contains("<ClInclude Include=\"") || line.contains("<ClCompile Include=\""))
         fileList << (parent + line.split("\"")[1]);
   }
   file.close();

   return fileList;
}

std::vector<Macro> ProjectAnalyzer::FindMacrosInFile(const QString& filePath)
{
   std::vector<Macro> macroBase;

   QFile file(filePath);
   if (!file.open(QIODevice::ReadOnly))
      return macroBase;
   QTextStream in(&file);
   while (!in.atEnd())
   {
      QString line = in.readLine();

      if (line.contains("#") && line.contains("define"))
      {
         Macro macro;
         macro.name = line.remove("#").remove("define").split(" ", QString::SkipEmptyParts).first();
         macroBase.push_back(macro);
      }
   }
   file.close();
   return macroBase;
}

ProjectAnalyzer::~ProjectAnalyzer()
{
}
