#pragma once

#include "Common\EXPORT.h"
#include "DataObjects/ProjectDataBaseObject/projectdatabaseobject.h"

#include <QObject>
#include <QString>

#include <vector>

class MACROCEPTIONCORE_EXPORT SolutionAnalyzer : public QObject
{
   Q_OBJECT

public:
    SolutionAnalyzer(QObject *parent = nullptr);
    ~SolutionAnalyzer();

    static std::vector<ProjectDataBaseObject> GetProjects(QString solutionPath);
};
