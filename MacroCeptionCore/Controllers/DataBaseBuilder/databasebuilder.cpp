#include "databasebuilder.h"

#include "Controllers\Analyzers\projectanalyzer.h"
#include "Controllers\Analyzers\solutionanalyzer.h"

DataBaseBuilder::DataBaseBuilder(QObject *parent)
    : QObject(parent)
{
}

DataBaseBuilder::~DataBaseBuilder()
{
}

SolutionDataBaseObject DataBaseBuilder::GenerateSolutionDataBase(QString solutionPath, QString title)
{
   SolutionDataBaseObject solutionBase(title, solutionPath);

   solutionBase.projectDataBaseObjects = SolutionAnalyzer::GetProjects(solutionPath);

   for(auto& proj : solutionBase.projectDataBaseObjects)
      proj.macros = ProjectAnalyzer::GetMacros(proj.path);

   for (const auto& proj : solutionBase.projectDataBaseObjects)
      for (const auto& macro : proj.macros)
         solutionBase.registeredMacros.push_back(macro);

   return solutionBase;
}
