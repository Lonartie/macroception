#pragma once

#include "Common\EXPORT.h"
#include "DataObjects\SolutionDataBaseObject\solutiondatabaseobject.h"

#include <QObject>
#include <QString>

class MACROCEPTIONCORE_EXPORT DataBaseBuilder : public QObject
{
   Q_OBJECT

public:
    DataBaseBuilder(QObject *parent);
    ~DataBaseBuilder();

    static SolutionDataBaseObject GenerateSolutionDataBase(QString solutionPath, QString title);
};
