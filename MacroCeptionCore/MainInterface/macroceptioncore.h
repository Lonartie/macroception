#pragma once

#include "Common/EXPORT.h"

#include <QString>
#include <QObject>

class MACROCEPTIONCORE_EXPORT MacroCeptionCore : public QObject
{
   Q_OBJECT

public:

   static const QString ResolveMacroFromFileRoot(const QString& filePath, const QString& projectPath, const QString& inputMacro);

   static const QString ExpandAllMacrosFromFile(const QString& filePath, const QString& projectPath, const QString& inputMacro);

   static const QString ResolveMacroFromProject(const QString& slnPath, const QString& inputMacro);

   static void GenerateSolutionDatabase(const QString slnPath, const QString title);

   static MacroCeptionCore& GetInstance();

signals:

   void SolutionDataBaseCreated(const QString title);

   void SolutionDataBaseCreationProgressChanged(const double progress);
};
