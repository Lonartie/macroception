#include "macro.h"

Macro::Macro(QObject *parent)
    : QObject(parent)
{
}

Macro::Macro(const Macro& obj)
{
   name = obj.name;
   resolvedPartialMacro = obj.resolvedPartialMacro;
   parameterNames = obj.parameterNames;
   parameterCount = obj.parameterCount;
}

Macro::~Macro()
{

}