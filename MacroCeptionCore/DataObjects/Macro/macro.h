#pragma once

#include "Common/EXPORT.h"

#include <QObject>
#include <QString>
#include <QStringList>

class MACROCEPTIONCORE_EXPORT Macro : public QObject
{
   Q_OBJECT

public:

   Macro(QObject* parent = nullptr);

   Macro(const Macro& obj);

   ~Macro();

   QString 
      name,
      resolvedPartialMacro;

   QStringList parameterNames;

   int parameterCount;
};
