#include "solutiondatabaseobject.h"

SolutionDataBaseObject::SolutionDataBaseObject(QString title, QString path, QObject* parent)
   : QObject(parent)
{
   this->title = title;
   this->path = path;
}

SolutionDataBaseObject::SolutionDataBaseObject(const SolutionDataBaseObject& obj)
{
   title = obj.title;
   path = obj.path;

   for (const auto& proj : obj.projectDataBaseObjects)
      projectDataBaseObjects.push_back(ProjectDataBaseObject(proj));

   for (const auto& macro : obj.registeredMacros)
      registeredMacros.push_back(Macro(macro));
}

SolutionDataBaseObject::~SolutionDataBaseObject()
{
}
