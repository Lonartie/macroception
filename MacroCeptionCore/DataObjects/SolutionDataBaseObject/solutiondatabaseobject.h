#pragma once

#include "Common/EXPORT.h"
#include "DataObjects\ProjectDataBaseObject\projectdatabaseobject.h"

#include <QObject>
#include <QString>
#include <vector>

class MACROCEPTIONCORE_EXPORT SolutionDataBaseObject : public QObject
{
   Q_OBJECT

public:

   SolutionDataBaseObject(QString title, QString path, QObject* parent = nullptr);

   SolutionDataBaseObject(const SolutionDataBaseObject& obj);

   ~SolutionDataBaseObject();

   QString
      title,
      path;

   std::vector<ProjectDataBaseObject> projectDataBaseObjects;

   std::vector<Macro> registeredMacros;
};
