#pragma once

#include "Common/EXPORT.h"
#include "DataObjects\Macro\macro.h"

#include <QObject>
#include <vector>

class MACROCEPTIONCORE_EXPORT ProjectDataBaseObject : public QObject
{
   Q_OBJECT

public:

   ProjectDataBaseObject(QString title, QString path, QObject* parent = nullptr);

   ProjectDataBaseObject(const ProjectDataBaseObject& obj);

   ~ProjectDataBaseObject();

   QString
      title,
      path;

   std::vector<Macro> macros;
};
