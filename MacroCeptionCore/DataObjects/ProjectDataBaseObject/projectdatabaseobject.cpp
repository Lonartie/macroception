#include "projectdatabaseobject.h"

ProjectDataBaseObject::ProjectDataBaseObject(QString title, QString path, QObject* parent)
   : QObject(parent)
{
   this->title = title;
   this->path = path;
}

ProjectDataBaseObject::ProjectDataBaseObject(const ProjectDataBaseObject& obj)
{
   title = obj.title;
   path = obj.path;
   for(const auto& macro : obj.macros)
      macros.push_back(Macro(macro));
}

ProjectDataBaseObject::~ProjectDataBaseObject()
{
}
